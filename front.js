function resizeCanvasToDisplaySize(canvas) {
    width = canvas.clientWidth;
    height = canvas.clientHeight;
    if (canvas.width !== width || canvas.height !== height) {
        canvas.width = width;
        canvas.height = height;
        return true;
    }
 
    return false;
}
function getCursorPosition(canvas, event) {
    var rect = canvas.getBoundingClientRect();
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;
    return {x:x,y:y};
}

const liquids = ['blox:water','blox:lava','blox:chocolate_liquid'];
const gases = ['blox:air','blox:poison_air'];

var noises = {};

function rnd(s, a, x, y, z){
    if(!noises[s]) noises[s] = new SimplexNoise({random:new Alea(s)});
    return (noises[s].noise((x||0)*a,(y||0)*a,(z||0)*a) * 0.5) + 0.5
}

class hitbox {
    constructor(x1, y1, x2, y2, action){
        if(x1!==undefined && x2!==undefined && y1!==undefined && x2!==undefined){
            this.x1 = x1/16;
            this.x2 = x2/16;
            this.y1 = y1/16;
            this.y2 = y2/16;
            this.action = action || null;
            // console.log(action);
            // console.log('banananana')
            this.check = function(x,y,e){
                let max = {i:0,j:0,n:0};
                let incValX = 0.001;
                let incValY = 0.001;
                if(e.velox < 0) incValX*=-1;
                if(e.veloy < 0) incValY*=-1;
                for(var i = 0;true;i+=incValX){
                    if(Math.abs(i) > Math.abs(e.velox)-0.002) i = e.velox;
                    for(var j = e.veloy;true;j-=incValY){
                        if(Math.abs(j) < 0.002) j = 0;
                        let a = {
                            top: e.y + e.height/16 + j,
                            bottom: e.y + j,
                            left: e.x-(e.width/32) + i,
                            right: e.x+(e.width/32) + i
                        }
                        let b = {
                            top: y + Math.max(this.y1,this.y2),
                            bottom: y + Math.min(this.y1,this.y2),
                            left: x + Math.min(this.x1,this.x2),
                            right: x + Math.max(this.x1,this.x2),
                        }
                        if(e.fake) console.log(i,j,a,b)
                        if(
                        //  a.left   >= b.right  || 
                        //  a.top    >= b.bottom || 
                        //  a.right  <= b.left   || 
                        //  a.bottom <= b.top
                            // e.x+i               <= x+this.x2 || 
                            // e.y+j+(e.height/16) <= y+this.y1 || 
                            // e.x+i+(e.width/16)  >= x+this.x1 || 
                            // e.y+j               >= y+this.y2
                            Math.ceil(a.bottom*100) >= b.top*100 ||
                            Math.floor(a.top*100) <= b.bottom*100 ||
                            Math.ceil(a.left*100) >= b.right*100 ||
                            Math.floor(a.right*100) <= b.left*100// ||
                            // a.bottom >= b.top ||
                            // a.top <= b.bottom ||
                            // a.left >= b.right ||
                            // a.right <= b.left
                        ) {
                            // console.log('d',i,j);
                            // if(i == 0){
                            //     console.log(e.x+i          >= x+this.x2, 
                            //         e.y+j+(e.height/16) >= y+this.y1,
                            //         e.x+i+(e.width/16)  <= x+this.x1, 
                            //         e.y+j          <= y+this.y2)
                            // }
                            // console.log(            
                            //     e.y+j+(e.height/16),y+this.y1,
                            //     e.y+j, y+this.y2
                            // )
                            if(Math.abs(i)+Math.abs(j) > max.n){
                                // console.log(Math.abs(j) >= Math.abs(max.j))
                                max = {
                                    i: i,
                                    j: max.j==e.veloy?max.j:j,
                                    n: Math.abs(i)+Math.abs(j),
                                    action:
                                        (
                                            Math.floor(a.bottom*160) == b.top*160 ||
                                            Math.ceil(a.top*160) == b.bottom*160 ||
                                            Math.ceil(a.left*16) == b.right*16 ||
                                            Math.floor(a.right*16) == b.left*16
                                        )?true:false
                                };
                                // console.log(a.bottom == b.top ||
                                //     a.top == b.bottom ||
                                //     a.left == b.right ||
                                //     a.right == b.left);
                                // i = 0;
                                j = 0;
                            }
                        }
                        if(j == 0) break;
                    }
                    if(i == e.velox) break;
                }
                // for(var vx = e.velox;vx != 0;vx-=0.001){
                //     if(b+vx-(e.width/32) <= Math.min(this.x1,this.x2) && b+vx+(e.width/32) <= Math.min(this.x1,this.x2)){
                //         break;
                //     }
                //     if(b+vx-(e.width/32) >= Math.max(this.x1,this.x2) && b+vx+(e.width/32) >= Math.max(this.x1,this.x2)){
                //         break;
                //     }
                //     if((e.velox < 0 && vx > 0) || (e.velox > 0 && vx < 0)){
                //         vx = 0;
                //         break;
                //     }
                //     if(vx<0)vx+=0.002;
                //     if(Math.abs(vx) < 0.001){
                //         vx = 0;
                //         break;
                //     }
                // }
                // for(var vy = e.veloy;vy != 0;vy-=0.001){
                //     if(b+vy < Math.min(this.y1,this.y2) && b+vy+(e.height/16) < Math.min(this.y1,this.y2)){
                //         break;
                //     }
                //     if(b+vy > Math.max(this.y1,this.y2) && b+vy+(e.height/16) > Math.max(this.y1,this.y2)){
                //         break;
                //     }
                //     if((e.veloy < 0 && vy > 0) || (e.veloy > 0 && vy < 0)){
                //         vy = 0;
                //         break;
                //     }
                //     if(vy<0)vy+=0.002;
                //     if(Math.abs(vy) < 0.001){
                //         vy = 0;
                //         break;
                //     }
                // }
                // if(vx == 0 || vy == 0){
                    // return {};
                // }
                // if(this.action)console.log(max.action)
                return {x:max.i,y:max.j,action: max.action!==false ? this.action : null};
            }
        } else if(x1){
            this.check = x1;
        }
    }
}

class block {
    constructor(id, hitboxes){
        this.id = id;
        this.image = document.getElementById(`block/${id}`);
        if(hitboxes){
            this.hitboxes = hitboxes;
        } else {
            this.hitboxes = [new hitbox(0,0,16,16)];
        }
    }
}

class tile {
    constructor(type){
        this.type = type;
    }
}

class region {
    constructor(x,y,h,w,l,s) {
        this.tiles = [];
        for(var i = 0;i < 16;i++){
            this.tiles[i] = [];
            // let r = rnd(s.slice(0,4).join(''),0.025,i+(x*16) - (w*8));
            // let rb = rnd(s.slice(4,6).join(''),0.025,i+(x*16) - (w*8));
            // let rd = rnd(s.slice(6,8).join(''),10,i+(x*16) - (w*8));
            // let stoneHeight = Math.floor(r * 16) - (y * 16) + (l*16);
            // let dirtHeight = stoneHeight + Math.floor(rb * 5) + 2;
            // let biomes = ['snow','grass','desert'];
            // let biome = biomes[Math.floor(((x*16)+i) / (w/biomes.length*16))];
            for(var j = 0;j < 16;j++){
                let bloc = 'blox:air';
                if(x < 0 || x >= w || y < 0 || y >= h){
                    bloc = 'blox:barrier'
                } //else {
                //     if(j < dirtHeight){
                //         if(biome == 'desert'){
                //             bloc = 'blox:sand';
                //         } else {
                //             bloc = 'blox:dirt';
                //         }
                //     }
                //     if(j < stoneHeight){
                //         bloc = 'blox:stone'
                //     }
                //     if(j == dirtHeight){
                //         if(biome == 'desert'){
                //             bloc = 'blox:sand';
                //         } else if(biome == 'snow') {
                //             bloc = 'blox:grass_snowy';
                //         } else {
                //             bloc = 'blox:grass';
                //         }
                //     }
                //     if(j == dirtHeight+1){
                //         if(biome == 'desert'){
                //             if(rd>0.8)bloc = 'blox:cactus'
                //             if(rd>0.9)bloc = 'blox:cactus_flower'
                //         } else if(biome == 'snow') {

                //         } else {
                //             if(rd>0.75)bloc= 'blox:sunflower'
                //             if(rd>0.8)bloc = 'blox:grass_plant'
                //             if(rd>0.9)bloc = 'blox:grass_plant_flower'
                //         }
                //     }
                //     // if(y == l && rnd == j){
                //     //     // console.log('planks');
                //     //     bloc = 'blox:wood_planks'
                //     // }
                // }
                this.tiles[i][j] = new tile(bloc);
            }
        }
    }

    setBlock(x,y){

    }
}

// class region {
//     constructor(x,y,h,w,l,s){
//         this.layers = [
//             new layer(x,y,h,w,l,s), //Backdrop
//             new layer(x,y,h,w,l,s), //Furniture
//             new layer(x,y,h,w,l,s), //Main
//             new layer(x,y,h,w,l,s), //
//             new layer(x,y,h,w,l,s)
//         ]
//     }
// }

class invItem {
    constructor(item,amount){
        this.amount = amount || (item?1:0);
        this.item = item || null;
    }
}

class item {
    constructor(id, maxStack, type, attackDamage, block){
        this.id = id;
        this.maxStack = maxStack;
        this.type = type || 'default';
        this.attackDamage = attackDamage || 3;
        this.image = document.getElementById('item/'+id);
        this.block = block || null;
    }
}

class inventory {
    constructor(size,preset){
        this.items = [];
        if(!size)size=32;
        if(!preset)preset=[];
        for(var i = 0;i < size;i++){
            this.items[i] = preset[i] || new invItem();
        }
        this.size = size;
    }
}

class player {
    constructor(name,x,y){
        this.name = name;
        this.inventory = new inventory(32, [new invItem('blox:pickaxe'),new invItem('blox:sword')]);
        this.health = 10;
        this.x = x;
        this.y = y;
        this.skin = Object.keys(skins)[0];
        this.facing = "right";
        this.velox = 0;
        this.veloy = 0;
        this.height = Math.max((skins[this.skin].height || 28),28);
        this.width = Math.max((skins[this.skin].width || 14),14);
        this.deathMessage = "died";
        this.onGround = false;
    }

    nextSkin(){
        this.skin = Object.keys(skins)[(Object.keys(skins).indexOf(this.skin)+1) % Object.keys(skins).length];
        this.height = skins[this.skin].height;
        this.width = skins[this.skin].width;
    }
}

class worldOptions {
    constructor({name, difficulty}){
        this.difficulty = difficulty;
        this.name = name;
    }
}

class world {
    constructor(height, width, options, seed) {
        this.height = height;
        this.width = width;
        this.regions = {};
        this.options = options;
        this.level = height/2;
        this.entities = [];
        // this.players = [];
        this.spawnx = width*8;
        this.spawny = 13 + (this.level*16);
        this.player = new player('MOBlox', this.spawnx, this.spawny);
        this.seed = Math.floor(seed || Math.random()*(65536*65536));
        // this.seeds = [Math.floor(this.seed / 65536), Math.floor(this.seed % 65536)];
        this.seeds = (this.seed).toString(2)
        while(this.seeds.length<32){
            this.seeds="0"+this.seeds;
        }
        
        for(var x = -1;x <= this.width;x++){
            for(var y = -1;y <= this.height;y++){
                this.regions[`${x}-${y}`] = new region(x,y,this.height,this.width,this.level,this.seeds);
            }
        }
        console.log('regions created')
        for(var x = 0;x < this.width*16;x++){
            let r =  rnd(parseInt(this.seeds.slice(0,8),2),  0.025, x - (this.width*8));
            let rb = rnd(parseInt(this.seeds.slice(8,12),2),  0.025, x - (this.width*8));
            let rd = rnd(parseInt(this.seeds.slice(12,16),2),  10,    x - (this.width*8));
            let stoneHeight = Math.floor(r * 16) + (this.level*16);
            let dirtHeight = stoneHeight + Math.floor(rb * 5) + 2;
            let biomes = ['snow','grass','desert'];
            let biome = biomes[ Math.floor( (x / 16) / (this.width / biomes.length) ) ];
            for(var y = 0;y < this.height*16;y++){
                let bloc = 'blox:air';
                if(x < 0 || x >= this.width*16 || y < 0 || y >= this.height*16){
                    bloc = 'blox:barrier'
                } else {
                    if(y < dirtHeight){
                        if(biome == 'desert'){
                            bloc = 'blox:sand';
                        } else {
                            bloc = 'blox:dirt';
                        }
                    }
                    if(y < stoneHeight){
                        bloc = 'blox:stone'
                    }
                    if(y == dirtHeight){
                        if(biome == 'desert'){
                            bloc = 'blox:sand';
                        } else if(biome == 'snow') {
                            bloc = 'blox:grass_snowy';
                        } else {
                            bloc = 'blox:grass';
                        }
                    }
                    if(y == dirtHeight+1){
                        if(biome == 'desert'){
                            if(rd>0.8)bloc = 'blox:cactus'
                            if(rd>0.9)bloc = 'blox:cactus_flower'
                        } else if(biome == 'snow') {

                        } else {
                            if(rd>0.75)bloc= 'blox:sunflower'
                            if(rd>0.8)bloc = 'blox:grass_plant'
                            if(rd>0.9)bloc = 'blox:grass_plant_flower'
                        }
                    }
                }
                setBlockAt(this,x,y,new tile(bloc));
            }
        }
        // noise.seed(this.seed);
        // console.log(0,0)
    }

    // newPlayer(name){0,-6
    //     this.players0,-6push(new Player(name));
    // }
}

var canvas, ctx, width, height,hud;
var skins = {
    'blox:moblox': {
        left: [
            document.getElementById('skin/moblox/1'),
            document.getElementById('skin/moblox/2'),
            document.getElementById('skin/moblox/3'),
            document.getElementById('skin/moblox/4')
        ],
        right: [
            document.getElementById('skin/moblox/1:flipped'),
            document.getElementById('skin/moblox/2:flipped'),
            document.getElementById('skin/moblox/3:flipped'),
            document.getElementById('skin/moblox/4:flipped')
        ],
        height: 28,
        width: 14
    },'blox:f1': {
        left: [
            document.getElementById('skin/f1/1'),
            document.getElementById('skin/f1/2'),
            document.getElementById('skin/f1/3'),
            document.getElementById('skin/f1/4')
        ],
        right: [
            document.getElementById('skin/f1/1:flipped'),
            document.getElementById('skin/f1/2:flipped'),
            document.getElementById('skin/f1/3:flipped'),
            document.getElementById('skin/f1/4:flipped')
        ],
        height: 29,
        width: 14
    }
}

var w = new world(64,64,new worldOptions({name:'Banana',difficulty:1}));

var images = {
    heart: document.getElementById('img/heart'),
    halfheart: document.getElementById('img/halfheart'),
    emptyheart: document.getElementById('img/emptyheart'),
    invbar: document.getElementById('img/invbar')
};

var frame = 0;
var fps = 0;
var lastZeroFrameTime = Date.now();

function cactusHurt(e){if(frame%30==0){e.health-=0.7;e.deathMessage="was pricked to death"}return e;}

blocks = {
    'blox:air': new block('blox:air',[]),
    'blox:stone': new block('blox:stone'),
    'blox:grass': new block('blox:grass'),
    'blox:dirt': new block('blox:dirt'),
    'blox:wood_log': new block('blox:wood_log'),
    'blox:wood_planks': new block('blox:wood_planks'),
    'blox:wood_wall': new block('blox:wood_wall',[]),
    'blox:barrier': new block('blox:barrier'),
    'blox:leaves': new block('blox:leaves',[]),
    'blox:grass_snowy': new block('blox:grass_snowy'),
    'blox:sand': new block('blox:sand'),

    'blox:grass_plant': new block('blox:grass_plant',[]),
    'blox:grass_plant_flower': new block('blox:grass_plant_flower',[]),
    'blox:sunflower': new block('blox:sunflower',[]),
    'blox:cactus': new block('blox:cactus',[new hitbox(5,0,10,15,cactusHurt),new hitbox(0,6,5,14,cactusHurt),new hitbox(10,1,16,7,cactusHurt)]),
    'blox:cactus_flower': new block('blox:cactus_flower',[new hitbox(5,0,10,15,cactusHurt),new hitbox(0,6,5,14,cactusHurt),new hitbox(10,1,16,7,cactusHurt)])
}
var items = {
    'blox:cactus': new item('blox:cactus', 64, 'block', 4, 'blox:cactus')
}

var zoom = 2;
var camera = {x:0,y:0};
var debug = false;

var pointers = {};

var chat = {
    open: false,
    message: ""
};

canvas = document.getElementById('canvas');
ctx = canvas.getContext('2d');
window.addEventListener('resize', function(event){
    resizeCanvasToDisplaySize(canvas);
});
resizeCanvasToDisplaySize(canvas);

function onPointerDown(device){

}
function onPointerMove(device){

}
function onPointerUp(device){

}

canvas.addEventListener("mousedown", function(event){
    pointers["mouse"] = {down: true, startTime: Date.now(), startx: getCursorPosition(canvas, event).x, starty: getCursorPosition(canvas, event).y}
    onPointerDown("mouse");
});
canvas.addEventListener("mousemove", function(event){
    if(!pointers["mouse"])pointers["mouse"] = {down: false, startTime: null, startx: null, starty: null};
    pointers["mouse"].x = getCursorPosition(canvas, event).x;
    pointers["mouse"].y = getCursorPosition(canvas, event).y;
    console.log('mmove')
    onPointerMove("mouse");
});
canvas.addEventListener("mouseup", function(event){
    pointers["mouse"].down = false;
    onPointerUp("mouse");
})
document.addEventListener("keydown", function(event) {
    keysdown[event.key] = true;
    if(chat.open) {
        // console.log(chat);
        if(event.key == "Escape"){
            chat.message = "";
            chat.open = false;
            return;
        } else if(event.key == "Enter"){
            if(chat.message.startsWith('/')){
                if(chat.message.startsWith('/tp')){
                    let args = chat.message.split(' ');
                    if(Number(args[1]) && Number(args[2])){
                        w.player.x = Number(args[1]);
                        w.player.y = Number(args[2]);
                        alert(`Teleported to ${w.player.x} ${w.player.y}`);
                    } else {
                        alert('Usage: /tp <x> <y>');
                    }
                } else {
                    alert('Invalid Command');
                }
            } else {
                alert(chat.message);
            }
            chat.message = "";
            chat.open = false;
        } else if(event.key.length == 1){
            if(keysdown["Shift"]){
                chat.message+=event.key.toUpperCase();
            } else {
                chat.message+=event.key
            }
        }
    } else if(event.key == "/" || event.key == "t"){
        chat.open = true;
        if(event.key == "/"){
            chat.message = "/";
        }
    } else if(event.key == "`"){
        debug = !debug;
    } else if(event.key == "p"){
        w.player.nextSkin();
    }
    // console.log(chat, event.key);
    event.preventDefault()
    return false;
})
document.addEventListener("keyup", function(event) {
    keysdown[event.key] = false;
    return false;
});
var keysdown = {};

function blockAt(i,j){
    if(!w.regions[`${Math.floor(i/16)}-${Math.floor(j/16)}`])w.regions[`${Math.floor(i/16)}-${Math.floor(j/16)}`]=new region(Math.floor(i/16),Math.floor(j/16),w.height,w.width,w.level,w.seeds);
    return w.regions[`${Math.floor(i/16)}-${Math.floor(j/16)}`].tiles[Math.abs(Math.floor(i))%16][Math.abs(Math.floor(j))%16]
}
function setBlockAt(w,i,j,s){
    if(!w.regions[`${Math.floor(i/16)}-${Math.floor(j/16)}`])w.regions[`${Math.floor(i/16)}-${Math.floor(j/16)}`]=new region(Math.floor(i/16),Math.floor(j/16),w.height,w.width,w.level,w.seeds);
    return w.regions[`${Math.floor(i/16)}-${Math.floor(j/16)}`].tiles[Math.abs(Math.floor(i))%16][Math.abs(Math.floor(j))%16] = s;
}

function calcVelo(t){
    // console.log('Calc');
    t.veloy-=0.01;
    if(Math.abs(t.velox) >= 0.02){
        if(t.velox>=0.02){
            t.velox -= 0.012;
        } else if(t.velox<=0.02) {
            t.velox += 0.012;
        }
    } else {
        t.velox = 0;
    }
    let minx = 1;
    let miny = 1;
    let actions = [];
    let hiit = false;
    for(var i = 0;i < 12;i++){
        // console.log('Calc',i)
        let a = i%3 - 1;
        let b = Math.floor(i/3)-1;
        let bl = blocks[blockAt(t.x+a,t.y+b).type];
        for(var j = 0;j < bl.hitboxes.length;j++){
            // console.log('Calc',i,j);
            let hit = bl.hitboxes[j];
            hiit = true;
            // console.log('Calc',i,j);
            let r = hit.check(Math.floor(t.x+a),Math.floor(t.y+b),t);
            if(r.action) actions.push(r.action);
            // console.log('Calc',i,j);
            if(Math.abs(r.x) < Math.abs(minx)) minx = r.x;
            if(Math.abs(r.y) < Math.abs(miny)) miny = r.y;
            // console.log('Calc',i,j);
        }
    }
    if(!hiit){
        minx = t.velox;
        miny = t.veloy;
    }
    if(minx > 0) t.facing = "right";
    if(minx < 0) t.facing = "left";
    if(minx == 0) {
        t.moving = false;
    } else {
        t.moving = true;
    }
    if(t.veloy == -0.01 && miny == 0){
        t.onGround = true;
    } else {
        t.onGround = false;
    }
    t.x += minx;
    t.y += miny;
    // if(Math.abs(t.veloy) > 0.03){
    //     t.health -= (Math.abs(t.veloy)-Math.abs(miny))*1
    // }
    // if(Math.abs(t.velox) > 0.02){
    //     t.health -= (Math.abs(t.velox)-Math.abs(minx))*1
    // }
    t.velox = minx;
    t.veloy = miny;
    for(var i = 0;i < actions.length;i++){
        // console.log(actions[i]);
        t=actions[i](t);
    }
    // console.log('FinCalc');
    return t;
}

function tick(){
    requestAnimationFrame(tick);
    frame = (frame+1)%60;
    if(frame == 0){
        fps = 60 / ((Date.now() - lastZeroFrameTime) / 1000);
        lastZeroFrameTime = Date.now();
        console.log(fps);
    }
    let speed = 0.012
    if( (keysdown.w||keysdown[" "]) && w.player.onGround) {
        w.player.veloy += 0.22;
    }
    if(keysdown.a) {
        if(Math.abs(w.player.velox) <= 0.22) w.player.velox -= 0.01;
        w.player.velox -= speed;
    }
    if(keysdown.d) {
        if(Math.abs(w.player.velox) <= 0.22) w.player.velox += 0.01;
        w.player.velox += speed;
    }
    camera = {
        x: (w.player.x - (width/32/zoom)),
        y: (w.player.y - (height/32/zoom))
    }
    if(w.player.x < 0 || w.player.x > w.width*16 || w.player.y < 0 || w.player.y > w.height*16){
        w.player.health = 0;
        w.player.deathMessage = "fell out of the world"
    }

    if(w.player.health < 0.5){
        alert(`Player ${w.player.deathMessage}. Respawning...`);
        keysdown = {};
        // w.player.respawn();
        w.player.health = 10;
        w.player.x = w.spawnx;
        w.player.y = w.spawny;
    }
    w.player = calcVelo(w.player);
    if(!w.regions[`${Math.floor(w.player.x/16)}-${Math.floor(w.player.y/16)}`]) w.regions[`${Math.floor(w.player.x/16)}-${Math.floor(w.player.y/16)}`] = new region(Math.floor(w.player.x/16),Math.floor(w.player.y/16),w.height,w.width,w.level,w.seeds);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    renderTiles();
    renderPlayer(w.player);
    renderEntities();
    renderHUD(w.player);
}
function renderTiles(){
    for(var i = camera.x-1;i < camera.x+width/16+1;i++){
        for(var j = camera.y-1;j < camera.y+height/16+1;j++){
            let r = blockAt(i,j);
            if(camera.x < 0) i-=1;
            if(camera.y < 0) j-=1;
            ctx.drawImage(
                blocks[r.type].image, 
                ( i - camera.x - ( camera.x % 1 ) ) * 16 * zoom, 
                height - (( j - camera.y - ( camera.y % 1 ) ) * 16 * zoom),
                16*zoom, 
                16*zoom
            );
            if(debug)ctx.strokeRect(
                ( i - camera.x - ( camera.x % 1 ) ) * 16 * zoom, 
                height - (( j - camera.y - ( camera.y % 1 ) ) * 16 * zoom),
                16*zoom, 
                16*zoom
            )
            if(camera.x < 0) i+=1;
            if(camera.y < 0) j+=1;
            // if(Math.floor(i) == 0 && Math.floor(j) == 0)console.log(camera.x, (i-Math.floor(camera.x))*16*zoom, (j-Math.floor(camera.y))*16*zoom);
        }
    }
}
function renderPlayer(player){
    // ctx.save();
    ctx.drawImage(
        skins[player.skin||'blox:moblox'][player.facing||"left"][player.moving?Math.floor(frame/60*skins[player.skin||'blox:moblox'][player.facing||"left"].length):0],
        // player.x*16*zoom, 
        // (player.y*16*zoom) - (56*zoom),
        width/2 - (player.width/2)*zoom,
        height/2 - ((player.height/2)-2)*zoom, 
        player.width*zoom, 
        player.height*zoom
    )
    if(debug)ctx.strokeRect(
        width/2 - (player.width/2)*zoom,
        height/2 - ((player.height/2)-2)*zoom, 
        player.width*zoom, 
        player.height*zoom
    );
    // ctx.restore();
}
function renderEntities(){

}
function renderHUD(p){
    let x = 10;
    let y = 10;
    let flashing = false;
    if(p.health < 2) flashing = true;
    for(var i = 0;i < 10;i++){
        while(x >= (canvas.width/2-25)){
            x = 10;
            y += 25;
        }
        drawHeart(x-(flashing?Math.random()/2:0),y-(flashing?Math.random()/2:0),0.2-(Math.random()/(flashing?90:200)),p.health-i);
        x+=25;
    }
    let ib = {
        left: (width - Math.min(400,width)) / 2,
        top: height - Math.min(55,width/(400/55)) - 10,
        width: Math.min(400,width), 
        height: Math.min(55,width/(400/55))
    }
    ctx.drawImage(images.invbar, ib.left, ib.top, ib.width, ib.height);
    // if(debug)ctx.strokeRect(x,y,100*s,100*s);
}

function drawHeart(x,y,s,fill){
    let image = images.emptyheart;
    if(fill >= 0.5){
        image = images.halfheart;
    }
    if(fill >= 1){
        image = images.heart;
    }
    ctx.drawImage(image, x, y, 100*s, 100*s);
    if(debug)ctx.strokeRect(x,y,100*s,100*s);
}

var requestAnimationFrame = window.requestAnimationFrame
    || window.webkitRequestAnimationFrame
    || window.mozRequestAnimationFrame
    || window.msRequestAnimationFrame
    || function(callback) { return setTimeout(callback, 1000 / 60); };

requestAnimationFrame(tick);